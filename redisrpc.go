// Package redisrpc is a simple library to support JSON-RPC calls using Redis
// instead of a server built into a Go app. This is useful when RPC servers
// should not bind ports due to firewall limitations, as outbound connections
// tend to be much more relaxed in such situations.
package redisrpc

import (
	"github.com/garyburd/redigo/redis"
)

type RedisRPC struct {
	// RpcIn is one or more Redis keys that hold RPCs.
	RpcIn []string

	// RpcOut is the redis key that will be used to write results of each RPC.
	RpcOut string

	// Timeout is the number of seconds to block while waiting for a new RPC. A
	// timeout value of 0 indicated an indefinite block.
	Timeout int

	// ClosePool will, when set to true, close the Redis connection pool when
	// the RedisRPC client is closed.
	ClosePool bool

	// pool is a connection pool used to communicate with the redis server
	// which will queue the RPCs invoked and their respective replies.
	pool *redis.Pool

	// yuckyRpcIn maintains a list of Redis keys that are checked for RPCs.
	// This allows us to easily watch many keys instead of being limited to
	// just one.
	yuckyRpcIn []interface{}

	// CurrentCmd is used to track the last RPC retrieved from redis.
	CurrentCmd string

	// remaining is used to track what's left to send from the Read method.
	remaining string

	// done will be true when there is no more data to read from CurrentCmd.
	done bool
}

// NewRedisRPC returns a new RedisRPC client for the given host
func NewRedisRPC(host, in, out string) RedisRPC {
	pool := redis.NewPool(func() (redis.Conn, error) {
		return redis.Dial("tcp", host)
	}, 3)

	rpc := NewRedisRPCPool(pool, in, out)
	rpc.ClosePool = true

	return rpc
}

// NewRedisRPCPool returns a new RedisRPC client using the specified Redis
// connection pool.
func NewRedisRPCPool(pool *redis.Pool, in, out string) RedisRPC {
	rpc := RedisRPC{
		pool:   pool,
		RpcOut: out,
	}
	rpc.SetRpcIn(in)

	return rpc
}

// SetRpcIn configures the RedisRPC client to wait for RPCs to enter any of the
// Redis lists whose keys are specified.
func (r *RedisRPC) SetRpcIn(keys ...string) {
	r.RpcIn = keys

	r.yuckyRpcIn = []interface{}{}
	for _, key := range keys {
		r.yuckyRpcIn = append(r.yuckyRpcIn, key)
	}

	// set the timeout for the blocking call
	r.yuckyRpcIn = append(r.yuckyRpcIn, r.Timeout)
}

// NextCommand will perform a blocking pop on the Redis lists whose keys match
// the RpcIn values.
func (r *RedisRPC) NextCommand() (string, error) {
	conn := r.pool.Get()
	defer conn.Close()

	cmd, err := redis.Strings(conn.Do("BRPOP", r.yuckyRpcIn...))
	if err != nil {
		return "", err
	}

	r.CurrentCmd = cmd[1]

	return r.CurrentCmd, nil
}

// Read returns all or a portion of the most recently retrieved RPC.
func (r *RedisRPC) Read(p []byte) (n int, err error) {
	if r.done {
		r.done = false

		// indicates that there's no more data to read
		return 0, nil
	} else if r.remaining == "" {
		r.remaining, err = r.NextCommand()
		if err != nil {
			return 0, err
		}
	}

	// read at most len(p) bytes from r.remaining
	for idx, c := range r.remaining {
		if idx >= len(p) {
			break
		}

		p[idx] = byte(c)
		n++
	}

	// remove the bytes we've already consumed from r.remaining
	r.remaining = r.remaining[n:]
	if r.remaining == "" {
		r.done = true
	}

	return n, nil
}

// Write pushes the result of an RPC onto the Redis list whose key matches the
// RpcOut value.
func (r *RedisRPC) Write(p []byte) (n int, err error) {
	return r.WriteTo(r.RpcOut, p)
}

// Write pushes the result of an RPC onto the Redis list with the specified
// key.
func (r *RedisRPC) WriteTo(key string, p []byte) (n int, err error) {
	conn := r.pool.Get()
	defer conn.Close()

	conn.Do("LPUSH", key, string(p))

	return len(p), nil
}

// Close releases resources used by the RedisRPC client.
func (r *RedisRPC) Close() error {
	if r.ClosePool {
		r.pool.Close()
	}

	return nil
}

===========
Go-RedisRPC
===========

RedisRPC is a simple library to support JSON-RPC calls using Redis instead of a
server built into a Go app. This is useful when RPC servers should not bind
ports due to firewall limitations, as outbound connections tend to be much more
relaxed in such situations.

API Documentation: http://godoc.org/bitbucket.org/codekoala/go-redisrpc

package main

import (
	"bitbucket.org/codekoala/go-redisrpc"
	"github.com/garyburd/redigo/redis"

	"errors"
	"log"
	"net/rpc"
	"net/rpc/jsonrpc"
)

var (
	// redis host to broker RPCs
	redisHost = ":6379"

	// commands is a list of commands to send to the RPC server. These are in
	// the same format as what the jsonrpc library issues under the hood.
	commands = []string{
		`{"method":"Arith.Multiply","params":[{"A":4,"B":5}]}`,
		`{"method":"Arith.Divide","params":[{"A":400,"B":53}]}`,
		`{"method":"Arith.Divide","params":[{"A":400,"B":0}]}`,
	}
)

type Args struct {
	A, B int
}

type Quotient struct {
	Quo, Rem int
}

type Arith int

func (t *Arith) Multiply(args *Args, reply *int) error {
	*reply = args.A * args.B
	return nil
}

func (t *Arith) Divide(args *Args, quo *Quotient) error {
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

func serveRequests() {
	log.Println("Starting RPC server...")
	conn := redisrpc.NewRedisRPC(redisHost, "rpc-in", "rpc-out")
	arith := new(Arith)

	server := rpc.NewServer()
	server.Register(arith)
	codec := jsonrpc.NewServerCodec(&conn)

	for {
		log.Println("Waiting for RPCs...")
		server.ServeCodec(codec)
	}
}

func main() {
	conn, err := redis.Dial("tcp", redisHost)
	if err != nil {
		log.Fatalln("Redis connection error:", err)
	}

	log.Println("Issuing RPCs before server is online...")
	for _, cmd := range commands {
		conn.Do("LPUSH", "rpc-in", cmd)
	}

	go serveRequests()

	for i := 0; i < len(commands); i++ {
		printReply(conn)
	}
}

func printReply(conn redis.Conn) {
	reply, err := redis.Strings(conn.Do("BRPOP", "rpc-out", 5))
	if err != nil {
		log.Fatalln("Redis reply error:", err)
	}
	log.Printf("Reply: %s", reply[1])
}
